#!/usr/bin/env perl

# File: rpn.pl
# Copyright Joey Kelly (joeykelly.net)
# December 27, 2019
# License: GPL version 2

use strict;
use warnings;

use feature qw(say switch);

my $help = <<'HELP';
rpn.pl is a trivial RPN calulator written in Perl by Joey Kelly.
If you don't know what Reverse Polish Notation is, this script isn't for you.
Please note that there is no interactivity and no stack. What you see is what you get.

Usage:

  ./rpn.pl 2 4 +
  6

  ./rpn.pl 2 4 *
  undef

    NOTE: because we're working on the *nix shell, the glob operator needs to be either quoted or escaped:
      ./rpn.pl 2 4 '*'
      8

      ./rpn.pl 2 4 \*
      8

Operators: + - * / %

HELP


my $first     = shift;
my $second    = shift;
my $operator  = shift;

# let's get our belt and suspenders out
$first    = '' unless defined $first;
$second   = '' unless defined $second;
$operator = '' unless defined $operator;

chomp $first;
chomp $second;
chomp $operator;

$first    = $first    =~ /^([-\.\d]+)$/       ? $1  : '';
$second   = $second   =~ /^([-\.\d]+)$/       ? $1  : '';
$operator = $operator =~ /^([+\-*\/\%^x]+)$/  ? $1  : '';

$first    = 'help'  unless $first;
$second   = ''      unless $second;
$operator = ''      unless $second;

# I guess we're satisfied our input is validated, so let's make some donuts
my $result;

if ($second and $operator) {
  $result = $first + $second    if $operator eq '+';
  $result = $first - $second    if $operator eq '-';
  $result = $first * $second    if $operator eq '*';    # you'll have to either escape or quote the operator
  $result = $first * $second    if $operator eq 'x';    # alternative operator
  $result = $first / $second    if $operator eq '/';
  $result = $first ** $second   if $operator eq '**';   # ditto above: escape or quote
  $result = $first ** $second   if $operator eq '^';    # alternative operator
  $result = $first % $second    if $operator eq '%';
}

$result = $help                 if $first eq 'help';

$result = "undef\nmaybe you want './rpn.pl help'?" unless defined $result;
say $result;
