# rpn.pl

rpn.pl is a trivial RPN calulator written in Perl by Joey Kelly.
If you don't know what Reverse Polish Notation is, this script isn't for you.
Please note that there is no interactivity and no stack. What you see is what you get.

Usage:

  ./rpn.pl 2 4 +

  6

  ./rpn.pl 2 4 *
  
  undef

    NOTE: because we're working on the *nix shell, the glob operator need to be either quoted or escaped:
      ./rpn.pl 2 4 '*'
      8

      ./rpn.pl 2 4 \*
      8

Operators: + - * / %
